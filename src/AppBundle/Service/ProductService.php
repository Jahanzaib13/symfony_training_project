<?php

namespace AppBundle\Service;
use AppBundle\Entity\Product;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Class ProductService
 * @package AppBundle\Service
 */
class ProductService
{
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em= $entityManager;
    }

    public function getShopProducts($shop_id){
        $em= $this->em;
        $shop=$em->getRepository("AppBundle:Shop")
            ->find($shop_id);
        $products= $em->getRepository("AppBundle:Product")
            ->findBy(["shop"=> $shop]);
        return $products;
    }

    public function saveShopProduct(Product $product, $shop_id, $file_name){
        $em= $this->em;
        $shop=$em->getRepository("AppBundle:Shop")
            ->find($shop_id);

        $new_product=new Product();
        $new_product->setName($product->getName());
        $new_product->setPrice($product->getPrice());
        $new_product->setQuantity($product->getQuantity());
        $new_product->setImage($file_name);
        $new_product->setShop($shop);

        $em->persist($new_product);
        $em->flush();

    }
}