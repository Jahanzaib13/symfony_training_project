<?php

namespace AppBundle\Service;

use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Class ShopService
 * @package AppBundle\Service
 */
class ShopService
{
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em= $entityManager;
    }

    public function saveUserShop(Shop $shop, User $user){
        $new_shop=new Shop();
        $new_shop->setShopId($shop->getShopId());
        $new_shop->setShopPassword($shop->getShopPassword());
        $new_shop->setUser($user);

        $em=$this->em;
        $em->persist($new_shop);
        $em->flush();

    }

    public function getUserShops(User $user){
        $em= $this->em;
        $shops = $em->getRepository("AppBundle:Shop")
            ->findBy(["user"=> $user]);
        return $shops;

    }
}