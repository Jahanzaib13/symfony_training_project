<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use AppBundle\Form\ShopFormType;
use AppBundle\Service\ShopService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ShopController
 * @package AppBundle\Controller
 */
class ShopController extends Controller
{
    /**
     * @Route("/getUserShops", name="get_user_shops")
     */
    public function userShopsAction(){

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em=$this->getDoctrine()->getManager();
        $shop_service=$this->get("app.service.shop_service");
        $shops= $shop_service->getUserShops($user);

        return $this->render("shop/showUserShopDetails.html.twig",[
            "shops" => $shops
        ]);
    }

    /**
     * @Route("saveUserShop", name="save_user_shop")
     */
    public function newShopAction(Request $request){

        $form=$this->createForm(ShopFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $shop=$form->getData();

            $shopService=$this->get("app.service.shop_service");
            $shopService->saveUserShop($shop, $user);

            $this->addFlash("success", "New shop created successfully..!");
            return $this->redirectToRoute("get_user_shops");
        }

        return $this->render(":shop:new.html.twig",[
            "shopForm" => $form->createView()
        ]);
    }
}