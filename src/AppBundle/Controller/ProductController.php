<?php

namespace AppBundle\Controller;

use AppBundle\Form\ProductFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 * @package AppBundle\Controller
 */
class ProductController extends Controller
{
    /**
     * @Route("shop/{shop_id}/products", name="shop_products")
     */
    public function getProductsAction($shop_id){
        $product_service= $this->get("app.service.product_service");
        $products= $product_service->getShopProducts($shop_id);

        return $this->render(":product:listShopProducts.html.twig",[
            "products" => $products,
            "shop_id" => $shop_id
        ]);
    }

    /**
     * @Route("/save/shop/{shop_id}/product", name="save_shop_product")
     */
    public function saveShopProduct(Request $request, $shop_id){
        $form=$this->createForm(ProductFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $product=$form->getData();
            $file = $form['image']->getData();
            $extension = $file->guessExtension();
            $file_name= rand(1,99999).'.'.$extension;
            if (!$extension) {
                $extension = 'bin';
            }
            $file->move("../web/images/product", $file_name);

            $product_service=$this->get("app.service.product_service");
            $product_service->saveShopProduct($product, $shop_id, $file_name);

            $this->addFlash("success", "New product created successfully..!");
            return $this->redirectToRoute("shop_products", array("shop_id" => $shop_id));
        }

        return $this->render(":product:new.html.twig",[
            "productForm" => $form->createView()
        ]);
    }
}