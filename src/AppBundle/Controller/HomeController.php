<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class HomeController
 * @package AppBundle\Controller
 */
class HomeController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(){
        if ($this->isGranted("ROLE_ADMIN")){
            return $this->redirectToRoute("easyadmin");
        }
        return $this->render("home/homePage.html.twig");
    }
}