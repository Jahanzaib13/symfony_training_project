<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="supplier")
 */
class Supplier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $supplierName;

    /**
     * @ORM\Column(type="integer")
     */
    private $supplierNumber;

    /**
     * @ORM\ManyToMany(targetEntity="Shop", inversedBy="suppliers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shops;

    public function __construct()
    {
        $this->shops = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getSupplierName():string
    {
        return $this->supplierName;
    }

    /**
     * @param mixed $supplierName
     */
    public function setSupplierName(string $supplierName)
    {
        $this->supplierName = $supplierName;
    }

    /**
     * @return mixed
     */
    public function getSupplierNumber()
    {
        return $this->supplierNumber;
    }

    /**
     * @param mixed $supplierNumber
     */
    public function setSupplierNumber(int $supplierNumber)
    {
        $this->supplierNumber = $supplierNumber;
    }

    /**
     * @return mixed
     */
    public function getShops()
    {
        return $this->shops;
    }

}